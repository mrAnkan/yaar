# Yet Another Augmented Reality Application
This is a take on trying to create a simple client side browser application using the visitors camera for a real-time augmented ocr experience. 
The idea is to first let the user scan a space for different texts to process with ocr, these strings are then stored together with spatial information from the sensors of the visitors device. When the visitor is done mapping the space, the next step is to enter a string to search for. The user can then use their device to navigate the space to find the string they are searching for.


## Planned features 
* Use device camera to get realtime feed of video to perform ocr on strings within a space
* Use device sensors to be able to show symbols on screen for navigating to a string within the space
* Potentially use a custom graph implementation to use for navigating between the strings within the space

## Current state
Early development, the following is done: basic files, files structure and skeleton for main application.

## Running for development
Run the following make-command `make serve-development` for using node to serve a simple https server on [htps://localhost:8443/index.html](htps://localhost:8443/index.html)