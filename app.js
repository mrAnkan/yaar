const fs = require('fs')
const http = require('http')
const https = require('https')
const certificate = fs.readFileSync('./private/certificate.pem', 'utf8')
const privateKey= fs.readFileSync('./private/key.pem', 'utf8')

const express = require('express')

const credentials = { key: privateKey, cert: certificate}
const app = express()

app.use(express.static('public'))

const httpServer = http.createServer(app)
const httpsServer = https.createServer(credentials, app)

app.get('*', function(req, res) {  
    res.redirect('https://' + req.headers.host + req.url)
})

httpServer.listen(8080)
httpsServer.listen(8443)