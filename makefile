serve-development:
ifeq (,$(wildcard ./private/key.pem))
	openssl \
	req \
	-newkey rsa:2048 -nodes \
	-keyout ./private/key.pem \
	-x509 -days 36500 -out ./private/certificate.pem \
	-subj "/C=US/ST=NRW/L=Earth/O=CompanyName/OU=IT/CN=www.example.com/emailAddress=email@example.com"
endif
	npm run start