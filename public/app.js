const { createWorker } = Tesseract

class Yaar {

    constructor() {
        this.videoWrapper = document.getElementById('video-wrapper')
        this.enableCameraButton = document.getElementById('enable-camera')
        this.cameraFeedElement = document.getElementById('camera-feed')

        this.enableCameraButton.addEventListener('click', this.enableCamera.bind(this))
        this.cameraFeedElement.addEventListener('loadeddata', this.handleCameraAnimationFrames.bind(this))

        this.renderedElements = []

        this.lastCallRender = 0
        this.worker = null
     }

    async initialize() {
        this.worker = createWorker({
            workerPath: './lib/tesseract/worker.min.js',
            langPath: './lang-data',
            corePath: './lib/tesseract-core/tesseract-core.wasm.js',
            logger: m => console.log(m),
        })
        
        await this.worker.load()
        await this.worker.loadLanguage('eng')
        await this.worker.initialize('eng')
    }

    async enableCamera(event) {
        event.preventDefault()

        event.target.classList.add('hidden')

        const constraints = {
            video: true
        }
        
        try {
            const stream = await navigator.mediaDevices.getUserMedia(constraints) 
            this.cameraFeedElement.srcObject = stream
        } catch (error) {
            console.log(error)
            alert('Could not use camera!')
        }
    }

    async handleCameraAnimationFrames() {

        for (const element of this.renderedElements) {
            this.videoWrapper.removeChild(element)
        }

        this.renderedElements.splice(0)

        const now = new Date()

        if (!this.lastCallRender || now - this.lastCallRender >= 2*1000) {
            this.lastCallRender = now

            const canvas = document.createElement("canvas")

            canvas.width = this.cameraFeedElement.videoWidth;
            canvas.height = this.cameraFeedElement.videoHeight;

            canvas.getContext('2d').drawImage(this.cameraFeedElement, 0, 0, canvas.width, canvas.height)
            
            const ocrResult = await this.worker.recognize(canvas.toDataURL())

            for (const line of ocrResult.data.lines) {
                const boundingBoxElement = document.createElement('div')

                boundingBoxElement.classList.add('highlight')
                boundingBoxElement.innerHTML = '&nbsp;'
    
                boundingBoxElement.style = `position: absolute; z-index: 2; top: ${line.bbox.y0}px; left: ${line.bbox.x0}px; width: ${line.bbox.x1 - line.bbox.x0}px; height: ${line.bbox.y0 - line.bbox.y1}px;`
                
                this.videoWrapper.appendChild(boundingBoxElement)
                this.renderedElements.push(boundingBoxElement)
            } 
        }

        window.requestAnimationFrame(this.handleCameraAnimationFrames.bind(this))
    }
}

const app = async () => {
    const yaar = new Yaar() 
    await yaar.initialize()
}
  
document.addEventListener("DOMContentLoaded", app)
  